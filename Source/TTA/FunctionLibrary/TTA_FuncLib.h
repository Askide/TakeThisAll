// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TTA_FuncLib.generated.h"

/**
 * 
 */
UCLASS()
class TTA_API UTTA_FuncLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};

USTRUCT(BlueprintType)
struct FPickUpItemInfo: public FTableRowBase
{
	GENERATED_BODY()

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Default")
		FName ItemName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Default")
		UStaticMesh* ItemStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Default")
		UTexture2D* ItemIcon = nullptr;
	
};
