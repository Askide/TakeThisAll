// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Engine/GameInstance.h"
#include "TTA/FunctionLibrary/TTA_FuncLib.h"
#include "TTAGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TTA_API UTTAGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " PickUp ")
		UDataTable* PickItemInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
		bool GetPickUpItemInfoByName(FName ItemName, FPickUpItemInfo& OutInfo);
	
};
