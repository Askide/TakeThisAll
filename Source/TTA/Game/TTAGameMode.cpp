// Copyright Epic Games, Inc. All Rights Reserved.

#include "TTAGameMode.h"
#include "TTAPlayerController.h"
#include "../Character/TTACharacter.h"
#include "UObject/ConstructorHelpers.h"

ATTAGameMode::ATTAGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATTAPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TTA/Blueprints/Character/BP_Character")); 
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	switch (rand() % 3)
	{
	case 0:
		GoalItemName = "Cylinder";
		break;
	case 1:
		GoalItemName = "Cube";
		break;
	case 2:
		GoalItemName = "Pyramid";
		break;
	default:
		break;
	}
	
}

void ATTAGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (TimeOutToWin > 0)
	{
		TimeOutToWin--;
		OnTimeChanged.Broadcast();
	}
	else
	{
		OnGameOver.Broadcast();
	}
	
}

void ATTAGameMode::GoalItemPickUp()
{
	GoalItemsCurrentAmount++;
	OnGoalItemPickUp.Broadcast();

	if (GoalItemsCurrentAmount == GoalItemTotalAmount)
	{
		OnGameWin.Broadcast();
	}
}

