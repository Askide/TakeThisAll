// Fill out your copyright notice in the Description page of Project Settings.


#include "TTAGameInstance.h"

bool UTTAGameInstance::GetPickUpItemInfoByName(FName ItemName, FPickUpItemInfo& OutInfo)
{
	bool bIsFind = false;
	FPickUpItemInfo* PickUpItemInfoRow;

	if (PickItemInfoTable)
	{
		PickUpItemInfoRow = PickItemInfoTable->FindRow<FPickUpItemInfo>(ItemName, "", false);
		if (PickUpItemInfoRow)
		{
			bIsFind = true;
			OutInfo = *PickUpItemInfoRow;
		}
	}
	return bIsFind;
}
