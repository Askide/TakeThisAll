// Copyright Epic Games, Inc. All Rights Reserved.

#include "TTAPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "../Character/TTACharacter.h"
#include "Kismet/GameplayStatics.h"

ATTAPlayerController::ATTAPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATTAPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// // keep updating the destination every tick while desired
	// if (bMoveToMouseCursor)
	// {
	// 	MoveToMouseCursor();
	// }
}

void ATTAPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// InputComponent->BindAction("SetDestination", IE_Pressed, this, &ATTAPlayerController::OnSetDestinationPressed);
	// InputComponent->BindAction("SetDestination", IE_Released, this, &ATTAPlayerController::OnSetDestinationReleased);

	// // support touch devices 
	// InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ATTAPlayerController::MoveToTouchLocation);
	// InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ATTAPlayerController::MoveToTouchLocation);
	//
	// InputComponent->BindAction("ResetVR", IE_Pressed, this, &ATTAPlayerController::OnResetVR);
}

void ATTAPlayerController::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

// void ATTAPlayerController::MoveToMouseCursor()
// {
// 	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
// 	{
// 		if (ATTACharacter* MyPawn = Cast<ATTACharacter>(GetPawn()))
// 		{
// 			if (MyPawn->GetCursorToWorld())
// 			{
// 				UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
// 			}
// 		}
// 	}
// 	else
// 	{
// 		// Trace to see what is under the mouse cursor
// 		FHitResult Hit;
// 		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
//
// 		if (Hit.bBlockingHit)
// 		{
// 			// We hit something, move there
// 			SetNewMoveDestination(Hit.ImpactPoint);
// 		}
// 	}
// }
//
// void ATTAPlayerController::SetNewMoveDestination(const FVector DestLocation)
// {
// 	APawn* const MyPawn = GetPawn();
// 	if (MyPawn)
// 	{
// 		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());
//
// 		// We need to issue move command only if far enough in order for walk animation to play correctly
// 		if ((Distance > 120.0f))
// 		{
// 			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
// 		}
// 	}
// }
//
// void ATTAPlayerController::OnSetDestinationPressed()
// {
// 	// set flag to keep updating destination until released
// 	bMoveToMouseCursor = true;
// }
//
// void ATTAPlayerController::OnSetDestinationReleased()
// {
// 	// clear flag to indicate we should stop updating the destination
// 	bMoveToMouseCursor = false;
// }
