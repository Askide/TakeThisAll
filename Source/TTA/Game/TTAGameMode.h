// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TTAGameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTimeChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGoalItemPickUp);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameOver);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameWin);

UCLASS(minimalapi)
class ATTAGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATTAGameMode();
	
	UPROPERTY(EditAnywhere, BlueprintAssignable, BlueprintReadWrite, Category = "Weapon")
		FOnTimeChanged OnTimeChanged;
	UPROPERTY(EditAnywhere, BlueprintAssignable, BlueprintReadWrite, Category = "Weapon")
		FOnGoalItemPickUp OnGoalItemPickUp;

	UPROPERTY(EditAnywhere, BlueprintAssignable, BlueprintReadWrite, Category = "Weapon")
	FOnGameOver OnGameOver;
	UPROPERTY(EditAnywhere, BlueprintAssignable, BlueprintReadWrite, Category = "Weapon")
	FOnGameWin OnGameWin;
	
	virtual void Tick(float DeltaSeconds) override;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GameRules")
	FName GoalItemName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GameRules")
	int GoalItemsCurrentAmount = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GameRules")
	int GoalItemTotalAmount;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="GameRules")
	int TimeOutToWin;

	UFUNCTION(BlueprintCallable)
	void GoalItemPickUp();
};



