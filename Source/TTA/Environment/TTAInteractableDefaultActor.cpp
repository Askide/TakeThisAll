// Fill out your copyright notice in the Description page of Project Settings.


#include "TTAInteractableDefaultActor.h"

// Sets default values
ATTAInteractableDefaultActor::ATTAInteractableDefaultActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATTAInteractableDefaultActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATTAInteractableDefaultActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATTAInteractableDefaultActor::Interact_Implementation()
{
	ITTA_IGameActor::Interact_Implementation();
}


